#!/bin/bash
import ROOT
import argparse, os, math, sys

#################################################################################################

def GetHistos(inputfile,htype,tag):######################################
#def GetHistos(inputfile,htype="num"): 
  #Dir=inputfile.GetDirectory('effHists_MyTurnOns')###################
  #keys = Dir.GetListOfKeys()########################################
  print(inputfile)
  keys = inputfile.GetListOfKeys()
  histograms = []
  #print(keys.GetName(),htype,tag)
  for key in keys:
    name = key.GetName()
    print(name,htype,tag)
    histo = ROOT.TH1F()
    if tag in name:
      if htype in name:
        #Dir.GetObject(name,histo)######################################
        #print(name)
        inputfile.GetObject(name,histo)
        histo.SetDirectory(0)
        histograms.append(histo)
  if len(histograms) == 0:
    print "Couldn't understand which histograms you want, try 'num' or 'denom'; exiting (GetHistos)"
    sys.exit(0)
  return histograms

#################################################################################################

def GetListOfStudies(inputfile):
  keys = inputfile.GetListOfKeys()
  studies = [key.GetName().split("_HLT")[0] for key in keys ] # study names will be repeated but there will ordered/paired with curve names
  return studies

#################################################################################################

def GetListOfTriggers(inputfile):
  #Dir=inputfile.GetDirectory('effHists_MyTurnOns')#
  #keys = Dir.GetListOfKeys()#
  
  keys = inputfile.GetListOfKeys()
  print(inputfile)
  for key in keys:
    print(key.GetName())
  triggers = [ "HLT"+ key.GetName().split("HLT_")[1] for key in keys if "num" in key.GetName() ]#
  #triggers = [ "L1"+ key.GetName().split("L1_")[1] for key in keys if "num" in key.GetName() ]#
  #triggers = [ "HLT"+ key.GetName().split("_HLT")[1] for key in keys if "num" in key.GetName() ]
  print("Triggers: "+str(triggers))
  reftriggers = [ "HLT" + key.GetName().split("HLT_")[1].split("_num")[0] for key in keys if "num" in key.GetName() ]#
  #reftriggers = [ "L1" + key.GetName().split("HLT_")[1].split("_num")[0] for key in keys if "num" in key.GetName() ]#
  #reftriggers = [ "HLT" + key.GetName().split("_HLT")[2].split("_num")[0] for key in keys if "num" in key.GetName() ]

  print("Triggers / Reference:")
  for line in range(len(triggers)): 
    print(triggers[line] + "  /  "+ reftriggers[line] )
  
  if len(triggers) != len(reftriggers):
    print "Something went wrong, different size in trigger lists, debug me please!"
    sys.exit(0)
  return triggers, reftriggers

#################################################################################################

def buildEfficiencyCurve(numerator,denominator,option='cl=0.683 b(1,1) mode',extraTag='',nrebin=1):#nrebin
  
  # Rebin histograms:########################### AGREGADO
  rebin=int(nrebin)
  numerator_rebin = numerator.Rebin(rebin)
  denominator_rebin = denominator.Rebin(rebin)
  ##############################################

  curve = ROOT.TGraphAsymmErrors(numerator_rebin,denominator_rebin,option)#AGREGADO _rebin
  #curve = ROOT.TGraphAsymmErrors(numerator,denominator,option)
  #print("CURVE="+str(curve))
  if extraTag!='':
    title = extraTag
  else:
    print(numerator.GetName().split("MyTurnOns_",2))
    Tit1= numerator.GetName().split("MyTurnOns_",2)[1].split("[0]_num",2)[0]
    Tit2= numerator.GetName().split("MyTurnOns_",2)[1].split("[0]_",2)[1]
    title= Tit1 + "-" + Tit2
    if nrebin !=1:
      title=title + "-rebin_"+str(rebin)
    print('Created curve:\t\t\t'+str(title))
    #title = numerator.GetName().split("_HLT",2)[0]+"_HLT" + numerator.GetName().split("_HLT",2)[1]

  curve.SetNameTitle(title,title)
  #curve.GetXaxis().SetTitle("leading jet p_{T} [GeV]")
  #curve.GetYaxis().SetTitle("Efficiency")
  return curve

#################################################################################################
def SetUpCanvas( setLogX = False):
  # For plotting purposes
  ROOT.gStyle.SetOptTitle(0)
  ROOT.gStyle.SetOptStat(0)
  can = ROOT.TCanvas("c","",1000,900)
  can.SetTopMargin(0.05)
  can.SetRightMargin(0.05)
  can.SetLeftMargin(0.05)
  if setLogX:
    can.SetLogX()

  ROOT.gPad.SetRightMargin(0.08)
  ROOT.gPad.SetTopMargin(0.15)
  ROOT.gPad.SetLeftMargin(0.15)
  return can

#################################################################################################
def SetUpCurve(curve,i):

  markers = [24,25,26,27,28]
  colors  = [ROOT.kBlack, ROOT.kBlue, ROOT.kRed+2, ROOT.kGreen+3]
  curve.SetMarkerColor( colors[i])
  curve.SetMarkerStyle( markers[i] )
  curve.SetLineColor(   colors[i] )
  
  return 

#################################################################################################
def GetGraphsAsymmErr(myInput,method):##########################method: TDT,Emulated
    keys = myInput.GetListOfKeys()
    GraphsAsymmErr = []
    #print(myInput,method)
    #print(keys)
    for key in keys:
        name = key.GetName()
        print(name, method, key)
        graph = ROOT.TGraphAsymmErrors()
        if method in name:
            #print ("method in name")
            myInput.GetObject(name,graph)
            GraphsAsymmErr.append(graph)

    if len(GraphsAsymmErr) == 0:
        print "Couldn't understand which method you want, try 'TDT' or 'Emulated'; exiting"
        sys.exit(0)
    #print(GraphsAsymmErr)
    return GraphsAsymmErr

#################################################################################################
def GetEfficiency(myInput,Eff,method):##########################method: TDT,Emulated
    print("")
    print(myInput,Eff,method)
    Met = GetGraphsAsymmErr(myInput,method)#Getea el TGraphAsymmErrors que tenga TDT en el nombre del archivo myInput(HelperFunctions)
    print(Met[0].GetN(),Met[0].GetY(),Met[0].GetX())
    print(Met[0])
    invGraph_Met = ROOT.TGraph(Met[0].GetN(),Met[0].GetY(),Met[0].GetX())#Hace un TGraph invertido (x,y)->(y,x)=(x',y')
    invGraph_Met.SetBit(invGraph_Met.kIsSortedX)#this helps the interpolation by telling Eval that points are sorted 
    xMet=invGraph_Met.Eval(Eff,0,"")#Evalua en el eje x'(y)=Eff y escribe el y'(x) correspondiente
    yMet=Met[0].Eval(xMet,0,"")#Recupera el valor de y
    #yMet=Eff
 
    return Met[0], xMet, yMet

#################################################################################################

def GetIQR(myInput,method):##########################method: TDT,Emulated    
    t, Eff84, y = GetEfficiency(myInput,0.84,method)
    t, Eff16, y = GetEfficiency(myInput,0.16,method)
    t, Eff50, y = GetEfficiency(myInput,0.50,method)
    IQR = (Eff84 - Eff16) / (2.0*Eff50)
    return IQR

#################################################################################################
def GetFracAtEff(myHisto,myInput,Eff,method,tag):
    
    # Get pt for Efficiency Requsted:
    t, EffReq, y = GetEfficiency(myInput,Eff,method)
    
    # Retrieve histograms:
    ''' 
    if method == "TDT" or method == "Emulated": 
        #numerator = GetHistos(myHisto,"num" + method)
        numerator = GetHistos(myHisto,"effHists_MyTurnOns_HLT_4j70_gsc130_boffperf_split-HLT_j0_perf_L1RD0_FILLED_pt[3]_num" + method)
        #numerator = HGetHistos(myHisto,"HLT_4j75_L14J20-HLT_j0_perf_L1RD0_FILLED_pt[3]_num" + method)

    elif method == "pt[0]" or method == "ET[0]":
        numerator = GetHistos(myHisto,method + "_num")
    '''
    numerator = GetHistos(myHisto, method,tag)
    print "numerator:\n"
    print(numerator)
    print(numerator[0])
    binmax = numerator[0].FindFixBin(EffReq) 
    Pass = numerator[0].Integral()
    Fail = numerator[0].Integral(0,binmax)
    FracAtEff = Fail/Pass

    return FracAtEff

#################################################################################################
def GetFracAtpt(myHisto,pt,method,tag):
    print(myHisto)
    # Retrieve histograms:
    '''
    if method == "TDT" or method == "Emulated":
        #numerator = GetHistos(myHisto,"num" + method)
        numerator = GetHistos(myHisto,"effHists_MyTurnOns_HLT_4j70_gsc130_boffperf_split-HLT_j0_perf_L1RD0_FILLED_pt[3]_num" + method)
        #numerator = HGetHistos(myHisto,"HLT_4j75_L14J20-HLT_j0_perf_L1RD0_FILLED_pt[3]_num" + method)
    elif method == "pt[0]" or method == "ET[0]":
        numerator = GetHistos(myHisto,method + "_num")
    '''
    numerator = GetHistos(myHisto, method,tag)
    binmax = numerator[0].FindFixBin(pt) 
    Pass = numerator[0].Integral()
    Fail = numerator[0].Integral(0,binmax)
    FracAtpt = Fail/Pass

    return FracAtpt


#################################################################################################
def GetFit(myInput,Eff,method,force):#,xmin,xmax

    print "##########################################"
    print "Performing preliminary fit: Error function"
    TGAE,pt_50,eff_50 = GetEfficiency(myInput,0.5,method)
    #print pt_50
    #print "Preliminary fit: overriding errors in efficiency curve"
    print("\npreliminary Fit for "+method)
    tge = ROOT.TGraphErrors()
    for p in range(0,TGAE.GetN()):
      tge.SetPoint(p,TGAE.GetX()[p],TGAE.GetY()[p])
      tge.SetPointError(p,.01,.01)
    
    xminfit = 0
    x_50eff = pt_50
    xmaxfit = 1000
    Rise    = pt_50-10
    
    #First fit is forced
    func = ROOT.TF1("fit","0.5+0.5*TMath::Erf((x-[2])/[3])",xminfit,xmaxfit)
    
    func.SetParName(2,"Turn-On (50%) [GeV]")
    func.SetParName(3,"Rise [GeV]")

    func.SetParameter(2,x_50eff) #in GeV
    func.SetParameter(3,Rise) #in GeV. No idea what this is yet4
    tge.Fit("fit","","", xminfit, xmaxfit)

    print "\nRetrieving some useful parameters from Preliminary fit:"
    Xat99    = func.GetX(Eff) # plateau
    print("Fit reaches "+str(Eff)+" efficiency at:\t"+str(Xat99))
    Yat0     = 0.5
    print("The offset of the fit is:\t"+str(Yat0))
    slope    = 0.5/func.GetParameter(3)*(2./ROOT.TMath.Pi())*1000.
    print("The slope of the fit is:\t"+str(slope))
    turnon   = func.GetParameter(2)
    print("Fit reaches 50\% efficiency at:\t"+str(turnon))
    rise     = func.GetParameter(3)
    print("Fit starts rising at:\t\t"+str(rise))

    Xat50    = func.GetX(0.5)
    Xat16    = func.GetX(0.16)
    Xat84    = func.GetX(0.84)
    prelimIQR      = (Xat84 - Xat16) / (2*Xat50)
    print "IQR for preliminary Fit:\t", prelimIQR







    print "\n\n##################################################################"
    print "Performing final fits: new fitting function with 3 free parameters"

    #xminfit = Xat99+100
    #xmaxfit = Xat99+400
    xminfit = turnon-70
    xminfit=Xat50
    #xminfit=50
    xmaxfit = turnon+160
    xmaxfit=Xat50+100

    
    # Fitting top part of the graph:
    if force == "True":
        print("Forcing!")
        func2 = ROOT.TF1("fit2","0.5+0.5*TMath::Erf((x-[1])/[2])",xminfit,xmaxfit)
        func2.SetParName(1,"Turn-On") # (50%) (GeV)")
        func2.SetParName(2,"Rise") # (GeV)")
        func2.SetParameter(1, turnon)
        func2.SetParameter(2, rise) # in GeV, takes the parameter returned by the preliminar fit
        tge.Fit("fit2","","", xminfit, xmaxfit)
    if force == "Nothing":
        print("Not forcing")
        func2 = ROOT.TF1("fit2","([3]-1)+(1+[0])/2+((1-[0])/2)*TMath::Erf((x-[1])/[2])",xminfit,xmaxfit)
        #func2 = ROOT.TF1("fit2","[3]+[4]*TMath::Log(x)+(1+[0])/2+((1-[0])/2)*TMath::Erf((x-[1])/[2])",xminfit,xmaxfit)
        
        func2.SetParName(0,"offset")
        func2.SetParName(1,"Turn-On") # (50%) (GeV)")
        func2.SetParName(2,"Rise") # (GeV)")
        func2.SetParName(3,"top")

        func2.SetParameter(0, 0.2)
        func2.SetParameter(1, turnon) # in GeV, takes the parameter returned by the preliminar fit
        func2.SetParameter(2, rise)   # in GeV, idem
        func2.SetParameter(3,1)
        tge.Fit("fit2","","", xminfit, xmaxfit)
        Eff=0.99*func2.GetParameter(3)
    if force == "Something":
        print("Not forcing")
        func2 = ROOT.TF1("fit2","([3]/2)+([3]/2)*TMath::Erf((x-[1])/[2])",xminfit,xmaxfit)
        #func2 = ROOT.TF1("fit2","[3]+[4]*TMath::Log(x)+(1+[0])/2+((1-[0])/2)*TMath::Erf((x-[1])/[2])",xminfit,xmaxfit)
        
        #func2.SetParName(0,"offset")
        func2.SetParName(1,"Turn-On") # (50%) (GeV)")
        func2.SetParName(2,"Rise") # (GeV)")
        func2.SetParName(3,"top")

        #func2.SetParameter(0, 0.2)
        func2.SetParameter(1, turnon) # in GeV, takes the parameter returned by the preliminar fit
        func2.SetParameter(2, rise)   # in GeV, idem
        func2.SetParameter(3,1)
        tge.Fit("fit2","","", xminfit, xmaxfit)
        Eff=0.99*func2.GetParameter(3)

    Xat99    = func2.GetX(Eff) # plateau
    
    print("Fit reaches "+str(Eff)+" efficiency at:\t"+str(Xat99))
    if force == "True":
        Yat0     = 0.5
        print("The offset of the fit is:\t"+str(Yat0))
        print("The mean of the fit is:\t"+str(((1-Yat0)/2)))
    if force == "Nothing":
        Yat0     = func2.GetParameter(0)
        print("The offset of the fit is:\t"+str((1+Yat0)/2))
        print("The mean of the fit is:\t"+str(((1-Yat0)/2)))
        Xat50    = func2.GetParameter(1)
        print("The fit reaches 50% at:\t"+str(Xat50))
        rise     = func2.GetParameter(2)
        print("The fit rises at:\t"+str(rise))
        top     = func2.GetParameter(3)
        print("The top limit is:\t"+str(top))
    if force == "Something":
        Yat0     = func2.GetParameter(3)/2
        print("The offset of the fit is:\t"+str((1+Yat0)/2))
        print("The mean of the fit is:\t"+str(((1-Yat0)/2)))
        Xat50    = func2.GetParameter(1)
        print("The fit reaches 50% at:\t"+str(Xat50))
        rise     = func2.GetParameter(2)
        print("The fit rises at:\t"+str(rise))
        top     = func2.GetParameter(3)
        print("The top limit is:\t"+str(top))

    Xat50    = func2.GetX(0.5)
    Xat16    = func2.GetX(0.16)
    Xat84    = func2.GetX(0.84)
    IQR      = (Xat84 - Xat16) / (2*Xat50)
    print "IQR for Fit:\t", IQR


    return func2,xminfit,xmaxfit,Xat99


