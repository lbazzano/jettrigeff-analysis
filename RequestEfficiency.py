# This program returns pt for a requested efficiency and calculates IQR of the curve by interpolating.
# To run  write python InterpolateTurnOn.py --input TurnOnFile.root --efficiency 0.95 --histograms AllSlices_pt.root
# input rootfile must contain two TurnOn curves: Emulated and TDT.
# histograms rootfile must contain four TurnOn curves: numerator and denominator for Emulated and TDT.
# Prints pt/efficiency, IQR and FracAtEff for both TDT and Emulated and saves a .eps for each in the current directory with this information. 
import ROOT
import argparse, os, math, sys
import array
import numpy as np
import AtlasStyle
import time
from HelperFunctions import GetEfficiency
from HelperFunctions import GetIQR
from HelperFunctions import GetFracAtEff
from HelperFunctions import GetFracAtpt
from HelperFunctions import GetFit



def Main(inputfile,Eff,histofile,pt,met,tag,doFit,force):#,xmin,xmax
    #################Open
    myInput = ROOT.TFile(inputfile,"read")  
    myHisto = ROOT.TFile(histofile,"read")  
    
    ##################TDT
    #method = "TDT"
    #method = "pt[0]"
    method = met[0]
    TDT, xTDT, yTDT                 = GetEfficiency(myInput,Eff,method)
    TDT_IQR                         = GetIQR(myInput,method)
    TDT_FracAtEff                   = GetFracAtEff(myHisto,myInput,Eff,method,tag)
    TDT_FracAtpt                    = GetFracAtpt(myHisto,pt,method,tag)
    TDT_Fit = False
    TDT_FracAtpt_Fit = False
    TDT_xmin = False
    TDT_xmax = False
    TDT_pt_99_fit = False
    if doFit == "True":
        TDT_Fit,TDT_xmin,TDT_xmax,TDT_pt_99_fit = GetFit(myInput,Eff,method,force)#,xmin,xmax
        TDT_FracAtpt_Fit                        = GetFracAtpt(myHisto,TDT_pt_99_fit,method,tag)
    
    #############Emulated
    #method = "Emulated"
    #method = "ET[0]"
    method = met[1]
    Emulated, xEmulated, yEmulated  = GetEfficiency(myInput,Eff,method)
    Emulated_IQR                    = GetIQR(myInput,method) 
    Emulated_FracAtEff              = GetFracAtEff(myHisto,myInput,Eff,method,tag)
    Emulated_FracAtpt               = GetFracAtpt(myHisto,pt,method,tag)
    Emulated_Fit = False
    Emulated_FracAtpt_Fit = False
    Emulated_xmin = False
    Emulated_xmax = False
    Emulated_pt_99_fit = False
    if doFit == "True":
        Emulated_Fit,Emulated_xmin,Emulated_xmax,Emulated_pt_99_fit = GetFit(myInput,Eff,method,force)#,xmin,xmax
        Emulated_FracAtpt_Fit                                       = GetFracAtpt(myHisto,Emulated_pt_99_fit,method,tag)


    ############Comparison on Fraction
    #This is to compare the fractions of usless event if pt at Eff is the same for both curves
    Delta_pt                        = xEmulated - xTDT
    Emulated_FracAtpt_plusDelta     = GetFracAtpt(myHisto,pt+Delta_pt,method,tag)
    
    ##############Trigger
    trigger=TDT.GetName().split("_pt")[0]
    
    ################Close
    myInput.Close()
    return xTDT,xEmulated,trigger,yTDT,yEmulated,TDT,Emulated,TDT_IQR, Emulated_IQR, TDT_FracAtEff, Emulated_FracAtEff, TDT_FracAtpt, Emulated_FracAtpt, Emulated_FracAtpt_plusDelta, TDT_Fit, Emulated_Fit, TDT_xmin, Emulated_xmin, TDT_xmax, Emulated_xmax,TDT_pt_99_fit,Emulated_pt_99_fit, TDT_FracAtpt_Fit, Emulated_FracAtpt_Fit


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--input",     dest='inputfile', required=True, default="",     help="input file")
    parser.add_argument("--efficiency",dest='Eff',                      default= 0.99,  help="wanted efficency")
    parser.add_argument("--histograms",dest='histofile', required=True, default="",     help="input histogram file")
    parser.add_argument("--tag",       dest='tag',       required=True, default="num",  help="tag of trigger en hist file")
    parser.add_argument("--pt",        dest='pt',                       default= 450,   help="pt for fraction calculation")
    parser.add_argument("--doFit",     dest='doFit',                    default="False",help="Do you want to perform fit? \"True\" or \"False\" ")
    #parser.add_argument("--xmin",      dest='xmin',                     default=0,    help="min fitting limit")
    #parser.add_argument("--xmax",      dest='xmax',                     default=1000, help="max fitting limit")
    parser.add_argument("--force",     dest='forceFit',                 default="False",help="Do you want to force fit? \"True\" or \"False\" ")
    parser.add_argument("--show",      dest="showPlot",                 default="False",help="Do yo want to see the plot now? \"True\" or \"False\" ")
    args = parser.parse_args()

    inputfile = args.inputfile
    histofile = args.histofile
    tag =       args.tag
    Eff =       args.Eff
    Eff =      float(Eff)
    pt =        args.pt
    pt =       float(pt)
    #Fit:
    doFit =     args.doFit
    #xmin =      args.xmin
    #xmin =     float(xmin)
    #xmax =      args.xmax
    #xmax =     float(xmax)
    forceFit =  args.forceFit
    showPlot =  args.showPlot

    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    ROOT.gErrorIgnoreLevel = ROOT.kError

    #tag = "effHists_MyTurnOns_HLT_4j70_gsc130_boffperf_split-HLT_j0_perf_L1RD0_FILLED_pt[3]_num"
    met = ["TDT","Emulated"]
    #met = ["Emulated","Emulated"]
    #met = ["pt[3]","ET[3]"]
    #met = ["pt","ET"]

    ##########################Run Main
    xTDT, xEmulated, trigger, yTDT, yEmulated, TDT, Emulated, TDT_IQR, Emulated_IQR, TDT_FracAtEff, Emulated_FracAtEff, TDT_FracAtpt, Emulated_FracAtpt, Emulated_FracAtpt_plusDelta, TDT_Fit, Emulated_Fit, TDT_xmin, Emulated_xmin, TDT_xmax, Emulated_xmax,TDT_pt_99_fit,Emulated_pt_99_fit,TDT_FracAtpt_Fit,Emulated_FracAtpt_Fit = Main(inputfile,Eff,histofile,pt,met,tag,doFit,forceFit)#,xmin,xmax
   
    ##############Print, Plot and Save
    TDT_list=[TDT,xTDT,yTDT,TDT_IQR,TDT_FracAtEff,TDT_FracAtpt,TDT_Fit,TDT_xmin,TDT_xmax,TDT_pt_99_fit,TDT_FracAtpt_Fit]
    Emulated_list=[Emulated,xEmulated,yEmulated,Emulated_IQR,Emulated_FracAtEff,Emulated_FracAtpt,Emulated_Fit,Emulated_xmin,Emulated_xmax,Emulated_pt_99_fit,Emulated_FracAtpt_Fit]
    List=[TDT_list,Emulated_list]
    c=[0,0]
    l=0
    #ROOT.gROOT.SetBatch(True)
    AtlasStyle.SetAtlasStyle()
    #astyle.ATLASLabel(0.2, 0.87, "Internal")

    print('\n'+str(TDT_list[0].GetName().split("_pt")[0])+':')
    for method in List:
        c[l]=ROOT.TCanvas("c"+str(l));
        c[l].SetGridx()
        c[l].SetGridy() 
       
        gr = method[0]
        #gr.SetLineColor(0)
        #gr.SetLineWidth(0)
        gr.SetMarkerColor(1)
        gr.SetMarkerSize(1)
        gr.SetMarkerStyle(20)
        #gr.SetFillColor(1)
        gr.SetTitle(str(gr.GetName()))
        '''
        try:
            trig=int(str(method[0].GetName()).split("j")[1].split("_")[0])
            gr.GetXaxis().SetLimits(trig-trig*0.5,trig+trig*0.5) 
        except:
            trig=int(str(method[0].GetName()).split("J")[1].split("_")[0].split("-")[0])
            #gr.GetXaxis().SetLimits(trig-trig*0.5,trig+trig*1.5) 
            gr.GetXaxis().SetLimits(trig,250) 
        '''
        gr.GetXaxis().SetLimits(0,1000) 

        #gr.Draw("ACP2")
        #gr.SetMarkerStyle(21)
        gr.Draw("ASP")
        #gr.Draw("aps")

        #gr.SetFillColor(2);
        #gr.SetFillStyle(3003);
        #gr.Draw("a2");
        #gr.Draw("p");

        if doFit == "True":
            fit = method[6]
            #fit.SetLineColor(2)
            #fit.SetLineWidth(2)
            #fit.Draw("same")
            fit.SetLineColor(2)
            fit.SetLineWidth(2)
            fit.SetRange(0,1000)
            fit.Draw("same")
        
            minx=ROOT.TLine(method[7],0,method[7],1)
            minx.SetLineStyle(2)
            minx.SetLineColor(2)
            minx.SetLineWidth(2)
            minx.Draw("same")
            maxx=ROOT.TLine(method[8],0,method[8],1)
            maxx.SetLineStyle(2)
            maxx.SetLineColor(2)
            maxx.SetLineWidth(2)
            maxx.Draw("same")
        
            Pfit=ROOT.TGraph(1,np.array(fit.GetX(Eff)),np.array(Eff))
            Pfit.SetMarkerSize(2);
            Pfit.SetMarkerColor(2)#4
            Pfit.SetMarkerStyle(29)#43#20
            Pfit.Draw("p")
            
            print('>>>>>>Fit: Reaches '+str(np.array(Eff))+' efficiency at pt = '+str(np.array("%.4f" % method[9]))+'[GeV]')
            print('>>>>>>Fit: Fraction of events at '+str(np.array(Eff))+' efficiency =  '+str(np.array("%.4f" % method[10])))


        myline=ROOT.TLine(np.array(method[1]),0,np.array(method[1]),np.array(method[2]))
        myline.SetLineStyle(1)
        myline.SetLineColor(30)
        myline.Draw("same")
        
        Point=ROOT.TGraph(1,np.array(method[1]),np.array(method[2]))
        Point.SetMarkerSize(2);
        Point.SetMarkerColor(30)#4
        Point.SetMarkerStyle(43)#43#20
        Point.Draw("p")


        print('\n'+str(met[l])+':')
        



        #Efficiency 
        print('Reaches '+str(np.array("%.4f" % method[2]))+' efficiency at pt = '+str(np.array("%.4f" % method[1]))+'[GeV]')
        #text_eff=ROOT.TLatex(np.array(method[1])+10,
        #                     np.array(method[2])-0.05,
        #                     "Efficiency ="+str(np.array("%.2f" % method[2]))+" at pt = "+str(np.array("%.2f" % method[1]))+" GeV")
        #text_eff.SetTextSize(0.03)
        #text_eff.Draw("same")
        
        #IQR
        print('IQR = '+str(np.array("%.4f" % method[3])))
	#text_IQR=ROOT.TLatex(np.array(method[1])+10,
        #                     np.array(method[2])-0.1,
        #                     "IQR = "+str(np.array("%.2f" % method[3])))
        #text_IQR.SetTextSize(0.03)
        #text_IQR.Draw("same")
        
        #Fraction at efficiency
        print('Fraction of events at '+str(np.array("%.4f" % method[2]))+" efficiency =  "+str(np.array("%.4f" % method[4])))
        #text_FAE=ROOT.TLatex(np.array(method[1])+10,
        #                     np.array(method[2])-0.15,
        #                     "Fraction at "+str(np.array("%.2f" % method[2])).split(".")[1]+" = "+str(np.array("%.2f" % method[4])))
        #text_FAE.SetTextSize(0.03)
        #text_FAE.Draw("same")
        
        #Fraction at pt
        print('Fraction of events at '+str(pt)+" GeV =  "+str(np.array("%.4f" % method[5]))+'\n')
        #text_Fpt=ROOT.TLatex(np.array(method[1])+10,
        #                     np.array(method[2])-0.2,
        #                     "Fraction at #it{p}_{T}="+str(pt)+" GeV = "+str(np.array("%.2f" % method[5])))
        #text_Fpt.SetTextSize(0.03)
        #text_Fpt.Draw("same")
        
        PDFName = str(method[0].GetName())+'-Efficiency_'+str(Eff).split(".")[1]
        c[l].Update()
        
        if showPlot == "True":
            raw_input()
            #time.sleep(10)
        c[l].Update() 
        c[l].Print(PDFName+".eps")
        l+=1
    
    #Comparison
    p = 0
    for method in List:
        print(met[p]+' reaches '+str(np.array("%.4f" % method[2]))+' efficiency at pt = '+str(np.array("%.4f" % method[1]))+'[GeV]')
        p+=1
    print("Difference = "+str(np.array("%.4f" % float(Emulated_list[1]-TDT_list[1])))+" GeV")
    #print('Compare the fraction of TDT useless events at ('+str(pt)+") GeV = "+str(np.array("%.4f" % TDT_FracAtpt)))
    #print('with the fraction of Emulated useless events at ('+str(pt)+" + "+str(np.array("%.4f" % float(Emulated_list[1]-TDT_list[1])))+") GeV = "+str(np.array("%.4f" % Emulated_FracAtpt_plusDelta)))
